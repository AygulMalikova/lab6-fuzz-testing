import calculateBonuses from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
  test('Valid program and valid amount', () => {
    const program = "Standard";
    const amount = 5000;

    expect(calculateBonuses(program, amount)).toEqual(0.05);
  });

  test('Valid program and boundary amount (10000)', (done) => {
    const program = "Diamond";
    const amount = 10000;

    assert.equal(calculateBonuses(program, amount),0.2*1.5);
    done();
  });

  test('Valid program and boundary amount (50000)', (done) => {
    const program = "Premium";
    const amount = 50000;

    assert.equal(calculateBonuses(program, amount),0.1*2);
    done();
  });

  test('Valid program and boundary amount (100000)', (done) => {
    const program = "Standard";
    const amount = 100000;

    assert.equal(calculateBonuses(program, amount),2.5*0.05);
    done();
  });

  test('Invalid program and valid amount', () => {
    const program = "Abcd";
    const amount = 25000;

    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('Valid program and amount > 100000', () => {
    const program = "Premium";
    const amount = 101000;

    expect(calculateBonuses(program, amount)).toEqual(0.25);
  });

  test('Invalid program and invalid amount', () => {
    const program = false;
    const amount = false;

    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('Empty program and empty amount', () => {
    const program = "";
    const amount = "";

    expect(calculateBonuses(program, amount)).toEqual(0);
  });
})
